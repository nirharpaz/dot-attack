﻿#pragma strict
var dotPrefub : GameObject[];

var maxInstRateInSec : float = 5f;

function Start () {

Invoke("dotInstantiation" ,maxInstRateInSec);	 

InvokeRepeating("IncreaseInstRate", 0f, 30f);

}

function Update () {

}

function DotInstantiation(){

	var instPos : Vector2 = new Vector2(Random.Range(-6.0f,6.0f), -7.0f);
	var instance : GameObject = Instantiate(dotPrefub[0], instPos, Quaternion.identity);
		
		NextDotInst();
}

function NextDotInst(){

	var instInSec : float;
	
		if(maxInstRateInSec >1f){
			instInSec = Random.Range(1f, maxInstRateInSec);
		}
		else
			instInSec = 1f;
		
		Invoke("DotInstantiation", instInSec);

	} 

function IncreaseInstRate(){
	
	if(maxInstRateInSec > 1f)
			maxInstRateInSec --;
			
	if(maxInstRateInSec == 1f)
		CancelInvoke("IncreaseInstRate");
}

