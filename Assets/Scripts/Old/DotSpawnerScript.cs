﻿using UnityEngine;
using System.Collections;

public class DotSpawnerScript : MonoBehaviour
{
    public GameObject[] dots;
    private float secondsUntilSpawning;

    // Use this for initialization
    void Start()
    {
        secondsUntilSpawning = 5f;
    }

    void DotSpawn()
    {
        Vector2 instPos = new Vector2(Random.Range(-6.0f, 6.0f), -7.0f);
        Instantiate(dots[0], instPos, Quaternion.identity);
        NextDotSpawn();
        secondsUntilSpawning = secondsUntilSpawning * 1.25f;
    }

    void NextDotSpawn()
    {
        Invoke("DotSpawn", secondsUntilSpawning);
    }

    public void stopSpawning()
    {
        CancelInvoke("DotSpawn");
    }
}