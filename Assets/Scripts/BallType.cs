﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallType : MonoBehaviour {
	public float ID; // should be given by instantiator
	public Color BallColor;
	public int BallIndex;

	//core variables
	protected float _Radius =3f;
	public int _ExplosionRank = 3;  //hard coded match 3. if raising from 3, update CheckRank(); should be red from a game manager

	public List <BallType> _NeighborsOfMyColor;
	public List <BallType> _AssignedNeighborsOfMyColor;
	public List <BallType> _NeighborsOfOtherColors;
	protected bool _ShouldExplode=false;
	public int _ColorRank =1;
	protected Transform _myTransform;
	protected SpriteRenderer _myRenderer;


	private int _BallLayerID =8;
		// Use this for initialization
	void Start () {
		ID = Random.Range (1, 99999);
		_myTransform = transform;
		_myRenderer = GetComponent<SpriteRenderer> ();
		if (_myRenderer != null)
			_myRenderer.color = BallColor;

		_Radius = GetComponent<CircleCollider2D> ().radius + 0.02f; //0.02 should be red from gama manager
	}


	// upon collision with other ball check new status based on my neighbores
	public void GetListOfNeighbores()
	{
		
		// overlapSphere to aquire all my neighbores
		Collider2D[] otherDots;
		Vector2 MyCenter = new Vector2 (_myTransform.position.x, _myTransform.position.y);
		MyCenter = new Vector2 (_myTransform.position.x, _myTransform.position.y);
		int ballLayerMask = 1 << _BallLayerID;
		otherDots = Physics2D.OverlapCircleAll(MyCenter, _Radius,ballLayerMask);

		// add all neighbores with my type to a list
		foreach (Collider2D dot in otherDots) {
			
			if (dot!=null && dot.gameObject != this.gameObject) {
				BallType dotType = dot.GetComponent<BallType> ();
				if (dotType.BallIndex == BallIndex) {
					if (!_NeighborsOfMyColor.Contains (dotType)) {
						_NeighborsOfMyColor.Add (dotType);
						_AssignedNeighborsOfMyColor.Add (dotType);
						dotType.AddNeighbor (this);
					}
				} else {
					_NeighborsOfOtherColors.Add (dotType);
					dotType.AddOddNeighbor (this);
				}
			}

		}
		// no neighbores - finish
		if (_NeighborsOfMyColor.Count == 0)
			return;

		//neighbores of my color
		CheckRank ();
	}



	protected void CheckRank()
	{
		// case 1: no neighbores - by rank remains 1, never entered to the function.
		
		int newRank = _ColorRank;
		Debug.Log ("i'm " + name +" " + ID + " and my rank is " + _ColorRank);	
		//hitting only 1 ball, aquiring last ball's rank
		if (_NeighborsOfMyColor.Count == 1 && _AssignedNeighborsOfMyColor.Contains(_NeighborsOfMyColor[0]) ) 
		{
			Debug.Log ("has 1 neighbor: " + _NeighborsOfMyColor [0]+" ID: "+ _NeighborsOfMyColor[0].GetID());	
			newRank += _NeighborsOfMyColor [0].GetRank();
			RemoveFromAssigned (0);
			_NeighborsOfMyColor [0].RemoveFromAssigned (this);
		}

		if (_NeighborsOfMyColor.Count == 2)
		{
			if (!(_NeighborsOfMyColor [0] == _NeighborsOfMyColor [1])) 
			{
				Debug.Log ("has 2 neighbors: " + _NeighborsOfMyColor [0] + " ID: " + _NeighborsOfMyColor [0].GetID () + " and " + _NeighborsOfMyColor [1] + " ID: " + _NeighborsOfMyColor [1].GetID ());
			
				if (!AreNeighbors (_NeighborsOfMyColor [0], _NeighborsOfMyColor [1])) {
					Debug.Log ("my neighbores are separated");
					newRank += (_NeighborsOfMyColor [0].GetRank () + _NeighborsOfMyColor [1].GetRank ());
					RemoveFromAssigned (0);
					if(_AssignedNeighborsOfMyColor.Count>=1)
						RemoveFromAssigned (0);

				} else
					newRank += _NeighborsOfMyColor [0].GetRank ();	
			} 
			else {
				Debug.Log ("has 1 neighbor: " + _NeighborsOfMyColor [0] + " ID: " + _NeighborsOfMyColor [1].GetID () + " that appears twice");
				_NeighborsOfMyColor.RemoveAt (1);

			}
		}
		SetRank (newRank);
		Debug.Log ("i'm " + name +" "  + ID + " and my new rank is " + newRank + ". no of Neighbors of my color detected: "+ _NeighborsOfMyColor.Count);	
	}

	public void SetRank(int newRank)
	{
		_ColorRank = newRank;
		foreach (BallType neighbor in _NeighborsOfMyColor) 
		{
			if (neighbor!=null && newRank != neighbor.GetRank ())
				neighbor.SetRank (newRank);
		}

	
		if (_ColorRank >= _ExplosionRank && !_ShouldExplode) {
//			Debug.Log("boom");
			Explode ();
		}
	}

	public void Explode()
	{
		_ShouldExplode = true;
		ReleaseNabors ();
		DestroyMyself ();

	}

	protected void DestroyMyself(){
//		Debug.Log ("upon destruction, my rank: " + _ColorRank + "my neighbor rank: " + _NeighborsOfMyColor[0].GetRank());
		foreach (BallType nbr in _NeighborsOfOtherColors) {
			RemoveOddNeighbor (this);
		}
		Destroy (this.gameObject);
		//GetComponent<DotMovement> ().ReleaseMe ();
		//_myTransform.position = new Vector3 (1000, 1000, 1000);

	}

	public int GetRank()
	{
		return _ColorRank;
	}



	public bool IsNeighbor(BallType other)
	{
		return (_NeighborsOfMyColor.Contains(other));
	}

	protected bool AreNeighbors (BallType A, BallType B)
	{
		return (A.IsNeighbor(B) || B.IsNeighbor(A));
	}

	public void AddNeighbor(BallType other)
	{
//		Debug.Log ("add nabor");
		if (!IsNeighbor (other)) {
			_NeighborsOfMyColor.Add (other);
			_AssignedNeighborsOfMyColor.Add (other);
		}
	}

	public void RemoveFromAssigned(BallType other)
	{
		_AssignedNeighborsOfMyColor.Remove (other);
	}

	public void RemoveFromAssigned(int index)
	{
		_AssignedNeighborsOfMyColor.RemoveAt (index);
	}

	public List<BallType> GetNeighbores()
	{
		return _NeighborsOfMyColor;
	}

	public void AddOddNeighbor(BallType other)
	{
//		Debug.Log ( ID +" adding nabor");
		if (!_NeighborsOfOtherColors.Contains (other) && !_NeighborsOfOtherColors.Contains(other))
			_NeighborsOfOtherColors.Add (other);


	}

	public void RemoveOddNeighbor(BallType other)
	{
		Debug.Log ("removing " + other.name);
		_NeighborsOfOtherColors.Remove (other);
	}

	public List<BallType> GetOddNeighbores()
	{
		return _NeighborsOfOtherColors;
	}

	protected void ReleaseNabors()
	{
		if (_NeighborsOfOtherColors.Count == 0)
			return;
		foreach (BallType other in _NeighborsOfOtherColors) {
			if (other != null) {
				RemoveOddNeighbor (this);
				ReleaseRec (other);
				//other.GetComponent<DotMovement> ().ReleaseMe (); //////needs to be recursive unless attached to center and without infinite loops
				//after releasing self, neighbor to releas has no neighbors that are paranted

			}
		}
	}

	public void ReleaseRec(BallType other)
	{
//		Debug.Log ("release rec ball no. " + other.ID);
		other.GetComponent<DotMovement> ().ReleaseMe ();
		foreach (BallType bt in other.GetOddNeighbores()) {
			if (bt != null && bt.tag == "Paranted")
				ReleaseRec (bt);
		}
		foreach (BallType bt in other.GetNeighbores()) {
			if (bt != null && bt.tag == "Paranted")
				ReleaseRec (bt);
		}

	}

	public float GetID()
	{
		return ID;
	}

	public void SetID(float id)
	{
		ID = id;
	}


}
