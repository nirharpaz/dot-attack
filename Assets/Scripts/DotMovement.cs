﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DotMovement : MonoBehaviour {
	public Transform _Center;
	private Transform _MyTransform;
	private BallType _MyBallType;
	public float _Speed; //should be readen from gama manager
	public bool _move;

	void Awake() {
		_MyTransform = transform;
		_move = true;
		_MyBallType = GetComponent<BallType> ();

	}

	//temmporary functions
	public void initCenter(Transform Center)
	{
		_Center = Center;
	}

	// Use this for initialization
	public void Init (Transform Center, float Speed, float MyID) {
		_Center = Center;
		_Speed = Speed;
		_MyBallType.SetID (MyID);
	}


	
	// Update is called once per frame
	void Update () {
		if (!_move)
			return;
		transform.position = Vector2.MoveTowards(_MyTransform.position, _Center.position, _Speed * Time.deltaTime);
	}

	void OnCollisionEnter2D(Collision2D other)
	{
		if(other.gameObject.tag == "Main" || other.gameObject.tag == "Paranted"){
			_move = false;
			//inst = false;
			//paranted = true;
			gameObject.tag = "Paranted";

			transform.parent = _Center.gameObject.transform;

			_MyBallType.GetListOfNeighbores ();
		}
	}

	public void ReleaseMe()
	{
//		Debug.Log ("release me");
		gameObject.tag = "Untagged";
		transform.parent = null;
		_move = true;
	}


}
