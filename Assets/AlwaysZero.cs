﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlwaysZero : MonoBehaviour {

	// Use this for initialization
	void Awake () {
		transform.localPosition = Vector3.zero;	
	}

}
